#
# Copyright (c) 1988 Regents of the University of California.
# All rights reserved.
#
# Redistribution and use in source and binary forms are permitted
# provided that the above copyright notice and this paragraph are
# duplicated in all such forms and that any documentation, advertising
# materials, and other materials related to such redistribution and
# use acknowledge that the software was developed by the University
# of California, Berkeley.  The name of the University may not be
# used to endorse or promote products derived from this software
# without specific prior written permission.  THIS SOFTWARE IS PROVIDED
# ``AS IS'' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
# WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF MERCHANTIBILITY AND
# FITNESS FOR A PARTICULAR PURPOSE.
#
# @(#)Makefile	5.1 (Berkeley) 10/18/88
#
CC=gcc
CFLAGS=	-O
SRCS=	login.c winpriv.c winsec.c lastlog.c
OBJS=	login.$(O) winpriv.$(O) winsec.$(O) lastlog.$(O)

LOGIN_OBJS = login.$(O) winpriv.$(O) winsec.$(O)
TEST_WINSEC_OBJS = test_winsec.$(O) winsec.$(O)

MAN=	login.1
prefix=/usr
docdir=/usr/share/doc/login
mandir=/usr/share/man
DESTDIR=
EXEEXT=.exe
O=o

all: login$(EXEEXT) lastlog$(EXEEXT) test_winsec$(EXEEXT)

login$(EXEEXT): $(LOGIN_OBJS)
	$(CC) -o $@ $(CFLAGS) $^ -lnetapi32

lastlog$(EXEEXT): lastlog.$(O)
	$(CC) -o $@ $(CFLAGS) $^

test_winsec$(EXEEXT): $(TEST_WINSEC_OBJS)
	$(CC) -o $@ $(CFLAGS) $^ -lnetapi32

clean:
	rm -f $(OBJS) test_winsec.$(O) core 
	rm -f login$(EXEEXT).stackdump login$(EXEEXT)
	rm -f lastlog$(EXEEXT).stackdump lastlog$(EXEEXT)
	rm -f test_winsec$(EXEEXT) test_winsec$(EXEEXT).stackdump

cleandir: clean
	rm -f $(MAN) tags .depend

depend: $(SRCS)
	mkdep -p $(CFLAGS) $(SRCS)

install: login$(EXEEXT)
	-mkdir -p $(DESTDIR)$(prefix)/bin $(DESTDIR)$(docdir) $(DESTDIR)$(mandir)/man1 $(DESTDIR)$(mandir)/man8
	install -m 4755 login$(EXEEXT) $(DESTDIR)$(prefix)/bin/login$(EXEEXT)
	install -m 0755 lastlog$(EXEEXT) $(DESTDIR)$(prefix)/bin/lastlog$(EXEEXT)
	install -m 644 login.README $(DESTDIR)$(docdir)/login.README
	install -m 644 login.1 $(DESTDIR)$(mandir)/man1/login.1
	install -m 644 lastlog.8 $(DESTDIR)$(mandir)/man8/lastlog.8

lint: $(SRCS)
	lint $(CFLAGS) $(SRCS)

tags: $(SRCS)
	ctags $(SRCS)

