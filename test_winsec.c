#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <getopt.h>
#include <errno.h>
#include <ctype.h>

extern int currentUserIsLocalSystem ();
extern int currentUserIsMemberOfLocalAdministrators ();
extern int uidIsLocalSystem (uid_t uid);
extern int uidIsMemberOfLocalAdministrators (uid_t uid);

static const char *PROGNAME = "test_winsec";
void usage (FILE * os, char *progname, int exitCode);
void help (FILE * os, char *progname, int exitCode);

int
main (int argc, char **argv)
{
  int opt_current = 0;
  int c;

  opterr = 0;
  while (1)
    {
      static struct option long_options[] = {
	{"help", no_argument, 0, 'h'},
	{"current", no_argument, 0, 'c'},
	{0, 0, 0, 0}
      };
      int option_index = 0;
      c = getopt_long (argc, argv, ":hc", long_options, &option_index);

      if (c == -1)
	break;

      switch (c)
	{
	case 'h':
	  help (stdout, argv[0], 0);
	  /* not reached */
	  break;
	case 'c':
	  opt_current = 1;
	  break;
	case ':':
	  fprintf (stderr, "option -%c missing required arguments\n", optopt);
	  usage (stderr, argv[0], 1);
	  /* not reached */
	  break;

	case '?':
	  {
	    if (isprint (optopt))
	      fprintf (stderr, "Unknown option `-%c'.\n", optopt);
	    else
	      fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
	    usage (stderr, argv[0], 1);
	  }
	  /* not reached */
	  break;

	default:
	  fprintf (stderr, "internal getopt error\n");
	  exit (1);
	}			/* switch */
    }				/* while */

  openlog ("test_winsec", LOG_ODELAY, LOG_AUTH);
  if (opt_current)
    {
      int rc;
      printf ("Current User -- ");
      rc = currentUserIsLocalSystem ();
      if (rc < 0)
	printf ("is LocalSystem: error %d\n", rc);
      else if (rc > 0)
	printf ("is LocalSystem: NO\n");
      else
	printf ("is LocalSystem: YES\n");

      printf ("Current User -- ");
      rc = currentUserIsMemberOfLocalAdministrators ();
      if (rc < 0)
	printf ("is BUILTIN\\Administrators member: error %d.\n", rc);
      else if (rc > 0)
	printf ("is BUILTIN\\Administrators member: NO\n");
      else
	printf ("is BUILTIN\\Administrators member: YES\n");
      printf ("\n");
    }
  while (optind < argc)
    {
      uid_t uid;
      int rc;
      long var;
      char *p;
      const char *val = argv[optind++];

      errno = 0;
      var = strtol (val, &p, 0);
      if (ERANGE == errno)
	{
	  fprintf (stderr, "ignoring out-of-range input '%s'\n", val);
	  continue;
	}
      else if (p == val)
	{
	  fprintf (stderr, "ignoring invalid numeric input '%s'\n", val);
	  continue;
	}
      else if (var < 0)
	{
	  fprintf (stderr, "ignoring negative uid (%ld): '%s'\n", var, val);
	  continue;
	}
      uid = (uid_t) var;

      printf ("User UID(%d) -- ", uid);
      rc = uidIsLocalSystem (uid);
      if (rc < 0)
	printf ("is LocalSystem: error %d\n", rc);
      else if (rc > 0)
	printf ("is LocalSystem: NO\n");
      else
	printf ("is LocalSystem: YES\n");

      printf ("User UID(%d) -- ", uid);
      rc = uidIsMemberOfLocalAdministrators (uid);
      if (rc < 0)
	printf ("is BUILTIN\\Administrators member: error %d.\n", rc);
      else if (rc > 0)
	printf ("is BUILTIN\\Administrators member: NO\n");
      else
	printf ("is BUILTIN\\Administrators member: YES\n");
    }
  closelog ();
}

void
usage (FILE * os, char *progname, int exitCode)
{
  const char *name = (progname ? progname : PROGNAME);
  FILE *s = (os ? os : stderr);
  fprintf (s, "Usage: %s [-hc] [uid [uid [uid ...]]]\n", name);
  exit (exitCode);
}

void
help (FILE * os, char *progname, int exitCode)
{
  static const char *helpText =
    "returns the full, official name of the operating system\n"
    "\n"
    "   --help|-h           print this help\n"
    "   --current|-c        print information about current user\n";

  const char *name = (progname ? progname : PROGNAME);
  FILE *s = (os ? os : stderr);

  fprintf (s, "%s [-hc] [uid [uid [uid ...]]]\n", name);
  fprintf (s, "%s\n", helpText);
  exit (exitCode);
}
